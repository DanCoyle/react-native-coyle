import React, { Component } from 'react';
import axios from "axios"; 
class Axios extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
  	var comp = this;
   	axios[this.props.method.toLowerCase()](this.props.url).then(function(response){
      comp.props.stateUpdate({...comp.props.state, ...response.data});
   	})
  }
  render(){
  	return null;
  }
}

export default Axios;