import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import { Container, Header, Left, Body, Right, Text, Button, Icon, Title,Subtitle } from 'native-base';
class HeaderComponent extends Component {

  render(){
    let leftContent = [], rightContent = [], body = [], left, right;

    if(this.props.title){
      body.push(<Title key='title'>{this.props.title}</Title>);
    } 
    if(this.props.subtitle){
      body.push(<Title key='subtitle'>{this.props.subtitle}</Title>);
    } 
    if(this.props.leftIcon){
      leftContent.push(<Icon key='leftIcon' onPress={() => this.props.navigation.navigate(this.props.leftIconNavigate)} name={this.props.leftIcon } /> ); 
    }
    if(this.props.leftText){
      leftContent.push(<Button transparent key='leftText' onPress={() => this.props.navigation.navigate(this.props.leftTextNavigate)}><Text>{this.props.leftText}</Text></Button>); 
    }
    if(this.props.rightIcon){
      rightContent.push(<Icon key='rightIcon' name={this.props.rightIcon } onPress={() => this.props.navigation.navigate(this.props.rightIconNavigate)} /> ); 
    }
    if(this.props.rightText){
      rightContent.push(<Button transparent key='rightText' onPress={() => this.props.navigation.navigate(this.props.rightTextNavigate)}><Text>{this.props.rightText}</Text></Button>); 
    }

    left = {leftContent};
    right = {rightContent};
    return (
    <Header transparent={this.props.transparent || false}>
      <Left>{left}</Left>
        <Body>{body}</Body>
        <Right>{right}</Right>
      </Header>
    );
  }
}

export default withNavigation(HeaderComponent);