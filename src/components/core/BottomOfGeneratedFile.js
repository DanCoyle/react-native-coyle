if(typeof(Data.navigationOptions) !== "undefined"){
  if(Data.navigationOptions.drawerLabel == "hide"){
    Data.navigationOptions.drawerLabel = <Hidden />;
  }
}

export default class GeneratedView extends Component {
    static navigationOptions = Data.navigationOptions || {drawerLabel:<Hidden/>};
    constructor(props) {
      super(props);
      this.stateUpdate = this.stateUpdate.bind(this);
      this.state = Data.state || {};
    }
    
    stateUpdate(data){ 
      this.setState(data);
    }


    render(){
      var comp = this;
      var out = [];
      for (var i = Data.blocks.length - 1; i >= 0; i--) {
        if(Data.blocks[i].component){
          out.push(<BasicDynamicComponent 
            state={comp.state} 
            stateUpdate={comp.stateUpdate}  
            key={pageName+Data.blocks[i].component} 
            comps={Components} {...Data.blocks[i]}/>)
        }
      }

     return (
          <Root>
            {out.reverse()}
          </Root>
      )
    }
}