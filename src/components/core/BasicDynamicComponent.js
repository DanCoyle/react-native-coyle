// import React, { Component, PureComponent  } from 'react' //Coming Back to this.
import React, { Component } from 'react'
import {Text,Toast} from 'native-base'
import { withNavigation } from 'react-navigation';
import {Platform} from 'react-native';
import axios from "axios"; 
const _ = require('lodash');

//// This is to decrease wasted re-renders "shouldComponentUpdate" or "PureComponent"?
  // if (process.env.NODE_ENV !== 'production') {
    // const {whyDidYouUpdate} = require('why-did-you-update');
    // whyDidYouUpdate(React);
  // }

export default withNavigation(class BasicDynamicComponent extends Component {

  constructor(props) {
    super(props);
  }

  handleStateUpdate(props){
    if(props.setstate){
      var action = props.setstate.action || "onPress";
      props[action] = (e) => {
        var fields = Object.keys(props.setstate);
        var obj = {};
        if(["onChange","onChangeText","onValueChange"].indexOf(action) > -1){
          for (var i = fields.length - 1; i >= 0; i--) {
            if(typeof(props.setstate[fields[i]]) == "string" && props.setstate[fields[i]].toLowerCase() == "#value"){
              if(typeof(props['device']) == "undefined" || props['device'] !== "web"){
                obj[fields[i].replace("@state.","")] = e;
              } else {
                obj[fields[i].replace("@state.","")] = e.target.value;
              }
              this.props.stateUpdate(obj)
            }
          }
        } else {
          for (var i = fields.length - 1; i >= 0; i--) {
              obj[fields[i].replace("@state.","")] = props.setstate[fields[i]];
          }
          this.props.stateUpdate(obj) 
        }
      }
    }
  }

  strings(propKey,props){
    var propKeyO = propKey+"_o";
    if(typeof(props[propKey]) == "string" &&
       (props[propKey].indexOf("@state.") > -1 || props[propKey].indexOf("@nav.") > -1 ) ){
       props[propKeyO] = props[propKey];
    }
    return propKeyO; 
  }

  handeStringReplacements(propKey,props){
    var comp = this;
    var propKeyO = this.strings(propKey,props);
    if(propKeyO.indexOf("_o_o") == -1){
        if(props[propKey] !== null &&
          typeof(props[propKey]) == "object"){
          var propPropKeys = Object.keys(props[propKey]);
          propPropKeys.forEach(function(propPropKey){
            comp.strings(propPropKey,props[propKey])
          })
        }

      }

      if(typeof(props[propKeyO]) !== "undefined"){
        var stringChecks = props[propKeyO].split(" ")
        var nString = [];
        for (var i = stringChecks.length - 1; i >= 0; i--) {
            var normalString = true;
            if(stringChecks[i].indexOf("@nav.") > -1){
                var nS = this.props.navigation.getParam(stringChecks[i].replace(new RegExp("@nav.", 'g'),""));
                nString = nString.concat(nS);
                normalString = false;
            }  
            if(stringChecks[i].indexOf("@state.") > -1){                
                var nS = stringChecks[i].replace(new RegExp("@state.", 'g'),"this.props.state.");
                nS = eval(nS);
                nString = nString.concat(nS);
                normalString = false;
            }   
            if(normalString == true) {
              nString = nString.concat(stringChecks[i])
            }
        }
          props[propKey] = nString.reverse().join(" ");
      }
  }

  subComponents(component,Components){
    let slot = [];
    if(component.subcomponents){
      for (var i = component.subcomponents.length - 1; i >= 0; i--) {
        var block =  this.slotComponent(component.subcomponents[i],Components);
        if(block !== false){
          slot.push(block);
        } else {
          slot.push(<Text>Component "{component.subcomponents[i].component}" Not Configured</Text>);
        }
      }
    } 
    if(slot.length > 0){
      return slot.reverse();
    }
    else{
      return false;
    }
  }

  handleDevice(props){
    if((typeof(props.device) !== "undefined")){
      if(props.device == "mobile" && ["ios","android"].indexOf(Platform.OS) == false){
        return null
      }
      if(props.device == "ios" && Platform.OS !== "ios"){
        return null
      }
      if(props.device == "android" && Platform.OS !== "android"){
        return null
      }
      if(props.device == "web" && Platform.OS !== "web"){
        return null
      }
    }
  }

  handleAxios(props){
    if(props.axios){
      var comp = this;
      var action = props.axios.action || "onPress";
      props[action] = () => {
        axios[props.axios.method.toLowerCase()](props.axios.url).then(function(response){
          comp.props.stateUpdate(response.data) 
        })
      }
    }
  }

  handleNavigation(props){
    if(props.navigate){
      if(typeof(props.navigate) == "string"){
        props.onPress = () => {
          this.props.navigation.navigate(props.navigate);
        }
      } else {
        var action = props.navigate.action || "onPress";
        props[action] = () => {
          this.props.navigation.navigate(props.navigate.screen,props.navigate.args);
        }
      } 
    }
  }

  handleDrawer(props){
    if(props.drawer){
      props[props.drawer] = () => {
          this.props.navigation.toggleDrawer();
        }
    }
  }

  handleToast(props){
    if(props.toast){
      var action = props.toast.action || "onPress";
      props[action] = () => {
        Toast.show(props.toast)
      }
    }
  }

  handleSelect(props){
     if(props.select){
        var action = props.select.action || "onPress";
        props[action] = () => {
          console.log(props.select)
        }
     }
  }

  slotComponent(component = null,Components){
    let props,slot,ResultComponent, device;
    if(component == null){
      component = this.props.component 

      props = _.clone(this.props,true);
    } else{
      props = _.clone(component,true);
      component = props.component;
    }
    if(Components[component]){
      if(this.handleDevice(props) === null){
        return null
      }

      var propKeys = Object.keys(props);
      for (var i = propKeys.length - 1; i >= 0; i--) {
          var propKey = propKeys[i];
          this.handeStringReplacements(propKey,props);
          this.handleStateUpdate(props);
          if(propKey == "currentValue"){
              if(props[propKey] == props["value"]){
                props["selected"] = true;
              } else {
                props["selected"] = false;
              }
          }
      }

      this.handleNavigation(props);
      this.handleDrawer(props);
      this.handleToast(props);
      this.handleSelect(props);
      this.handleAxios(props);

      var subSlot =this.subComponents(props,Components);
      if(subSlot){
        slot = subSlot;
      } else{
        slot = props.slotOriginal || props.slot || null
      }
      ResultComponent = Components[component];
      ResultComponent = <ResultComponent key={props.component+"_"+props.key_name} {...props}>{slot}</ResultComponent>;
      return ResultComponent;
    } else{
      return false;
    }
  }

  render(){
    var results = this.slotComponent(null,this.props.comps);
    return results
  }
})
