import React, { Component } from 'react';
import { Text, View } from 'react-native';
import Hidden from '../core/Hidden.js';
export default class HelloWorldApp extends Component {
	static navigationOptions = 	{
      "title":"Custom Screen",
      drawerLabel:<Hidden/>
  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text>Hello, world!</Text>
        <Text> This is a custom screen located at</Text>
        <Text>"src/Views/HelloWorld.js"</Text>
      </View>
    );
  }
}