import RadioButton from "./dataComponents/RadioButton.js";
import LikertScale from "./dataComponents/LikertScale.js";
import NavOptions from "./dataComponents/NavOptions.js";
import HeaderComp from "./dataComponents/HeaderComp.js";
import TextInput from "./dataComponents/TextInput.js";
import Button from "./dataComponents/Button.js";
import Text from "./dataComponents/Text.js";
import H1 from "./dataComponents/H1.js";
import TemplateList from "./dataComponents/TemplateList.js";
import SearchBar from "./dataComponents/SearchBar.js";
import SubSearchLinks from "./dataComponents/SubSearchLinks.js";

export default {
	navigationOptions:NavOptions("Choose A Person"),
	state:{links:[]},
	blocks:[
		HeaderComp("",["Back","ChoosePerson"]),
		H1("Give Feedback to @nav.name"),
		Text("Choose a template"),
		SearchBar(),
		SubSearchLinks(
			[
				{label:"Suggested"},
				{label:"All"},
				{label:"Favorites"},
			]
		),
		{
			component:"ScrollView",
			style:{marginTop:20},
			subcomponents:TemplateList(
				[
					 {"label":"General", "args":{} },
					 {"label":"Presentation", "args":{} },
					 {"label":"Meeting", "args":{} },
					 {"label":"Workshop", "args":{} },
					 {"label":"Conversation", "args":{} },
					 {"label":"Prospect Meeting", "args":{} },
					 {"label":"Sales Demo", "args":{} },
					 {"label":"1-on-1", "args":{} },
					 {"blank":true} 
				] 
			)
		}
	]
}