import LinkList from "./dataComponents/LinkList.js";
import NavOptions from "./dataComponents/NavOptions.js";
import HeaderComp from "./dataComponents/HeaderComp.js";
import Button from "./dataComponents/Button.js";
import Text from "./dataComponents/Text.js";
import SearchBar from "./dataComponents/SearchBar.js";

const Styles = {
	btn:{
		marginRight:10,
	}
}

export default {
	navigationOptions:NavOptions("Choose A Person"),
	blocks:[
	HeaderComp("",{
			right:{
				icon:"close",
				route:"Home"
			}
		}),
	{
		"component":"H1",
		"slot":"Who?",
	},
	SearchBar(),
	{
		"component":"ScrollView",
		'horizontal':true,
		subcomponents:[
			Button("Suggested",{style:Styles.btn, transparent:true}),
			Button("All",{style:Styles.btn, transparent:true}),
			Button("Favorites",{style:Styles.btn, transparent:true}),
			Button("My Groups",{style:Styles.btn, transparent:true}),
			Button("Org Groups",{style:Styles.btn, transparent:true}),
		]
	},
	{
    "component":"ScrollView",
	 subcomponents:LinkList([
	 		{
	 			title:"Alec",
	 			route:{screen:"TemplateSelect",args:{name:"Alec"}},
	 		},
	 		{
	 			title:"Alex",
	 			route:{screen:"TemplateSelect",args:{name:"Alex"}},
	 		},
	 		{
	 			title:"Dan",
	 			route:{screen:"TemplateSelect",args:{name:"Dan"}},
	 		},
	 		{
	 			title:"Natalie",
	 			route:{screen:"TemplateSelect",args:{name:"Natalie"}},
	 		},
	 		{
	 			title:"Robin",
	 			route:{screen:"TemplateSelect",args:{name:"Robin"}},
	 		},
	 	])
	}]
}