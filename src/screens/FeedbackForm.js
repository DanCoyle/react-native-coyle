import LinkList from "./dataComponents/LinkList.js"
import Image from "./dataComponents/Image.js"
import Text from "./dataComponents/Text.js"
import Row from "./dataComponents/Row.js"
import Col from "./dataComponents/Col.js"
import Grid from "./dataComponents/Grid.js"
import H1 from "./dataComponents/H1.js"
import Button from "./dataComponents/Button.js"
import RadioButton from "./dataComponents/RadioButton.js"
import NavOptions from './dataComponents/NavOptions.js'
import HeaderComp from './dataComponents/HeaderComp.js'
import ScrollView from './dataComponents/ScrollView.js'

export default {
	navigationOptions:NavOptions("",false),
	state:{
		buttonValue1:null,
		buttonValue2:null,
	},
	blocks:[
	HeaderComp("",{right:{
				icon:"close",
				route:"Home"
			}}),
	H1("Give feedback to @nav.name"),
	ScrollView(
		[
		Row([
			Col([
				Text("Was the content presented focused?"),
				RadioButton("@state.buttonValue1",[
					{
						label:"Yes",
						value:"yes"
					},
					{
						label:"No",
						value:"no"
					},
				]),
			]),
		]),
		Row([
			Col([
				Text("Was the Audience engaged?"),
				RadioButton("@state.buttonValue2",[
					{
						label:"Yes",
						value:"yes"
					},
					{
						label:"No",
						value:"no"
					},
				])
			])
		]),
		Row([
			Col([
				Text("Was are their strengths while presenting?"),
				{component:"Item",
				style:{
					flex:1,
					flexWrap:'wrap',
					flexDirection:"row",
				},
				subcomponents:[
					Button("Eye Contact",{light:true, style:{
						marginTop:10,
						marginRight:5,
					}}),
					Button("Tone of Voice",{light:true, style:{
						marginTop:10,
						marginRight:5,
					}}),
					Button("Concentration",{light:true, style:{
						marginTop:10,
						marginRight:5,
					}}),
					Button("Enthusiam",{light:true, style:{
						marginTop:10,
						marginRight:5,
					}}),
					Button("Storytelling",{light:true, style:{
						marginTop:10,
						marginRight:5,
					}}),
				]}
			])
		]),
		Row([
			Col([
				Text("Comments"),
					{
						component:"Textarea",
						rowSpan:5,
						'bordered':true,
						'placeholder':'Enter text right here...'
					}
				])
		]),
		Row([
			Col([
				Button("Give Feedback",{style:{
					justifyContent:"center", 
					textAlign: 'center'}})
			],{style:{
				padding:10, 
				paddingBottom:100
			}})
		])
		],{style:{padding:25}}),
	]}