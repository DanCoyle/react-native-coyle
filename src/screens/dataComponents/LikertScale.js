export default function(labels,rating, args = null){
	return {
		"component":"Content",
		"key_name":"likert_scale",
		"subcomponents":[
			{
				"component":"Text",
				key_name:"ratingText",
				"slot":"@state.labels[@state.rating-1]",
				style:{
					marginTop:50,
				}
			},
			{
				"component":"Slider",
				key_name:"slider",
				"setstate":{action:"onValueChange","rating":"#value"},
				step:1,
				"maximumValue":"@state.labels.length",
				minimumValue:1,
				style:{
					marginBottom:50
				}
			},
		]
	}
}