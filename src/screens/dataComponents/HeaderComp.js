function HeaderComp(title, args = null){
	var leftRoute = null;
	var leftLabel = null;
	var leftIcon = null;
	var rightRoute = null;
	var rightLabel = null;
	var rightIcon = null;
	var drawer = null;
	var transparent = args.transparent || true;
	var color = "white";
	if(transparent == true){
		color = "black";
	}

	if(typeof(args.left) !=="undefined"){
		drawer = args.left.drawer || false;
		leftRoute = args.left.route || null;
		leftLabel = args.left.label || null;
		leftIcon = args.left.icon || null;
	}
	
	if(typeof(args.right) !=="undefined"){
		rightRoute = args.right.route || null;
		rightLabel = args.right.label || null;
		rightIcon = args.right.icon || null;
	}

	var data = 	{
			"component":"Header",
			transparent:transparent,
			subcomponents:[
				{
					component:"Left",
					subcomponents:[
						{
							component:"Button",
							transparent:transparent,
							navigate: leftRoute,
							drawer:drawer,
							subcomponents:[
								{
									component:"Icon",
									style:{color:color},
									name:leftIcon
								},
								{
									component:"Text",
									style:{color:color},
									slot:leftLabel
								}
							]
						}
					]
				},
				{
					component:"Body",
					subcomponents:[
						{
							component:"Title",
							style:{color:color},
							slot:title
						}
					]
				},
				{
					component:"Right",
					subcomponents:[
						{
							component:"Button",
							transparent:transparent,
							navigate: rightRoute,
							subcomponents:[
								{
									component:"Icon",
									style:{color:color},
									name:rightIcon
								},
								{
									component:"Text",
									style:{color:color},
									slot:rightLabel
								}
							]
						}
					]
				}
			]

		}

		if(args !== null){
			data = {...data, ...args}
		}
		return data;
}

export default HeaderComp;