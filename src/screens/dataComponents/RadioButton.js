function RadioButton(stateValue,labels){
	var out = [];
	var stateValueKey = stateValue.replace("@state.","")
	for (var i = labels.length - 1; i >= 0; i--) {
		var left = {
						"component":"Left",
						subcomponents:[
							{
								component:"Text",
								"slot":labels[i].label,
								"setstate":{action:"onPress", [stateValueKey]: labels[i].value}
							}
						]
					};
		var right = {
						"component":"Right",
						subcomponents:[
							{
								component:"Radio",
								"selected":false,
								"currentValue":stateValue,
								"value":labels[i].value || "value needs to be defined",
								"setstate":{action:"onPress", [stateValueKey]: labels[i].value}
							}
						]
					};
		var item = {
			   "component":"ListItem",
			   'key_name':labels[i].label.replace(/ /,''),
			   "buttonGroupValue":stateValue,
			   "behavior":"radio",
				subcomponents:[
					left,
					right
				]
			};
		out.push(item);
	}

	var data = {
		"component":"Content",
		"subcomponents":out.reverse()
	};
	return data;
}
export default RadioButton