import Button from "./Button.js";

export default function(links,args = null){
	var buttons = []
	links.forEach((link) =>{
		buttons.push(Button(link.label,{transparent:true}))
	})
	var data = {
		component:"Item",
		subcomponents:[
			{
				"component":"ScrollView",
				'horizontal':true,
				subcomponents:buttons
			},
			{
				component:"Badge",
				success:true,
				style:{
					marginTop:10
				},
				subcomponents:[
				{
					component:"Text",
					slot:"+"
				}
				]
			}	
		]
	}

	if(args !== null){
		data = {...data, ...args}
	}
	return data;
}