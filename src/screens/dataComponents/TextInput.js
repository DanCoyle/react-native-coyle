export default function(placeholder, args = null){
	var data  =	{
				"component":"Input",
				"placeholder":placeholder,
			}

	if(args !== null){
		data = {...data,...args};
	}
	return data;
}