import Text from "./Text.js";
export default function(text,action = null,args = null){
	var data =  {
		"component":"Button",
		key_name:"button",
		"subcomponents":[
			Text(text)
		],
	}

	if(action !== null){
		data = {...data, ...action}
	}
	if(args !== null){
		data = {...data, ...args}
	}
	return data;
}