export default function(subcomponents,args = null){
	var data =  {
		"component":"Row",
		"subcomponents":subcomponents,
		style:{marginTop:20}
	}

	if(args !== null){
		data = {...data, ...args}
	}
	return data;
}