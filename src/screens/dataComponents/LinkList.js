

function LinkList(Links){
	var array = [];
	for (var i = Links.length - 1; i >= 0; i--) {
		var item = 
				{
				component:"ListItem",
				"key_name":Links[i].route,
				navigate:Links[i].route,
				subcomponents:[
					{
						component:"CardItem",
						subcomponents:[
							{
								component:"Row",
								"subcomponents":[
									{
										component:"Col",
										size:3,
										subcomponents:[
											{
											component:"Text",
											"slot":Links[i].title,
											},
										]
									},
									{
										component:"Col",
										size:1,
										subcomponents:[
											{
												component:"Icon",
												"name":Links[i].icon || "arrow-forward",
												style:{
													textAlign: 'right',
													alignSelf: 'stretch'
												},
											},
										]
									}
								]
							},
						]
					}
				]
			};
			if(typeof(Links[i].args) !== "undefined"){
				item = {...item,...Links[i].args}
			}
			array.push(item)
	}
	return array.reverse();
}

export default LinkList;