const style = {
	body:{
		flex:1
	},
	listItem:{
		fontsize:20,
	}
};


function UserList(Users){
	var array = [];
	for (var i = Users.length - 1; i >= 0; i--) {
		var item = 
				{
				component:"ListItem",
				style:style.listItem,
				navigate:Users[i].route,
				subcomponents:[
					{
						component:"CardItem",
						subcomponents:[
							{
								component:"Icon",
								"name":"arrow-forward"
							},
							{
								component:"View",
								"subcomponents":[
									{
										component:"Text",
										"slot":Users[i].title,
										"style":{
											"fontSize":style.fontsize,
										}
									},
								]
							}
						]
					}
				]
			};
			array.push(item)
	}
	return array;
}

export default UserList;