

function SelectList(option){
	var array = [];
	for (var i = option.length - 1; i >= 0; i--) {
		var item = 
				{
				component:"ListItem",
				"key_name":option[i].value,
				subcomponents:[
					{
						component:"Item",
						select:{action:"onPress", value:option[i].value},
						subcomponents:[
							{
								component:"Row",
								"subcomponents":[
									{
										component:"Col",
										size:3,
										subcomponents:[
											{
											component:"Text",
											"slot":option[i].title,
											},
										]
									},
									{
										component:"Col",
										size:1,
										subcomponents:[
											{
												component:"CheckBox",
												checked:true,
												style:{
													textAlign: 'right',
													alignSelf: 'stretch'
												},
											},
										]
									}
								]
							},
						]
					}
				]
			};
			if(typeof(option[i].args) !== "undefined"){
				item = {...item,...option[i].args}
			}
			array.push(item)
	}
	return array.reverse();
}

export default SelectList;