export default function(subcomponents,args = null){
	var data =  {
		"component":"Col",
		"subcomponents":subcomponents,
	}

	if(args !== null){
		data = {...data, ...args}
	}
	return data;
}