export default function(slot,args = null){
	var data  =	{
				component:"H1",
				"slot":slot,
				"style":{
					padding:10
				}
			}
			
	if(args !== null){
		data = {...data,...args};
	}
	return data;
}