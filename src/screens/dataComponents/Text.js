export default function(slot,args = null){
	var data  =	{
				component:"Text",
				"slot":slot
			}
			
	if(args !== null){
		data = {...data,...args};
	}
	return data;
}