import Text from "./Text.js";
import Button from "./Button.js";
import Row from "./Row.js";
import Col from "./Col.js";

const Styles = {
	templateBtn:{
		flex:1,
		height:100,
		justifyContent: 'center',
		alignItems: 'center'
	}
}
export default function(templates,action = null,args = null){

	var data =  []
	var i = 0;
	var total = 0;
	var row = [];
	templates.forEach((template) => {
		if(template.blank == true){
			row.push(Col());
		} else{
			row.push(Col([
				Button(template.label,{style:Styles.templateBtn,light:true, navigate:{
					screen:"FeedbackForm",
					args:{name:"@nav.name"}
				}}),
				],
				{
					style:{padding:5} 
				} 
			));
		}
		i = i + 1;
		total = total + 1;
		if(i == 3 || total == templates.length){
			i = 0;
			data.push(Row(row))
			row = [];
		}
	})

	return data;
}