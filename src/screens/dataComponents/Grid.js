export default function(subcomponents,args = null){
	var data =  {
					component:"Grid",
					subcomponents:subcomponents
				}

	if(args !== null){
		data = {...data, ...args}
	}
	return data;
}