export default function(uri = null,style = null ,args = null){
	uri = uri === null ? "https://outmatch.com/wp-content/uploads/2018/09/dm-logo.png" : uri; //TODO: Replace with image not found image.
	var data  =	{
		"component":"Image",
		"key_name":"image",
		"style":style || {"height": 200, "width": 200, "flex": 1},
		"source":{"uri": uri}
	}

	if(args !== null){
		data = {...data, ...args}
	}

	return data
}