export default function(subcomponents,args = null){
	var data =  {
					component:"ScrollView",
					subcomponents:subcomponents
				}

	if(args !== null){
		data = {...data, ...args}
	}
	return data;
}