export default function(subcomponents,args = null){
	var data =  {
		"component":"Item",
		rounded:true,
		subcomponents:[
			{
				component:"Icon",
				name:'Home'
			},
			{
				component:"Input",
				placeholder:"Search"
			},
		]
	}

	if(args !== null){
		data = {...data, ...args}
	}
	return data;
}