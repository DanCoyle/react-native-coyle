export default function(title,show = false,args = null){
	var data = {
		title: title,
	}
	if(show == false){
		data["drawerLabel"] = "hide"
	}
	if(args !== null){
		data = {...data,...args};
	}
	return data;
}