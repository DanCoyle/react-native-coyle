import LinkList from "./dataComponents/LinkList.js"
import Image from "./dataComponents/Image.js"
import Text from "./dataComponents/Text.js"
import NavOptions from './dataComponents/NavOptions.js'
import HeaderComp from './dataComponents/HeaderComp.js'
import Button from './dataComponents/Button.js'
export default {
	navigationOptions:NavOptions("Home",true),
	state:{ip:"",name:"Cool Guy"},
	blocks:[
		HeaderComp(null,{
			transparent:true,
			left:{
				icon:'menu',
				drawer:"onPress"
			}}),
		{
			component:"Container",
			subcomponents:[
				{
					component:"Content",
					subcomponents:[
					Text("Home Screen")
					]
				},
				{
					component:"Footer",
					style:{backgroundColor:"black"},
					subcomponents:[
						{
							component:"FooterTab",
							subcomponents:[
							{
								component:"Button",
								vertical:true,
								subcomponents:[
								{
									component:"Icon",
									name:"home"
								},
								Text("Home")
								]
							},
							]
						},
						{
							component:"FooterTab",
							subcomponents:[
							{
								component:"Button",
								vertical:true,
								subcomponents:[
								{
									component:"Icon",
									name:"mail"
								},
								Text("Request")
								]
							},
							]
						},
						{
							component:"FooterTab",
							subcomponents:[
								{
									component:"Button",
									navigate:"GiveFeedback",
									vertical:true,
									subcomponents:[
									{
										component:"Icon",
										name:"compass"
									},
									Text("Give")
									]
								},
							]
						},
					]
				}
			]
		}		
	]
}