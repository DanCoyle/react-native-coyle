import LinkList from "./dataComponents/LinkList.js"
import Image from "./dataComponents/Image.js"
import Text from "./dataComponents/Text.js"
import NavOptions from './dataComponents/NavOptions.js'
import HeaderComp from './dataComponents/HeaderComp.js'
export default {
	navigationOptions:NavOptions("Give Feedback",true),
	state:{ip:"",name:"Cool Guy"},
	blocks:[
		HeaderComp(null,{
			right:{
				icon:"close",
				route:"Home"
			}
		}),
		{
			"component":"H1",
			"slot":"Where do you want to give feedback?"
		},
		{
		"component":"ScrollView",
		"subcomponents": LinkList(
			[
				{
					title:"People",
					route:"ChoosePerson",
				},
				{
					title:"Process",
					route:"Process",
				},
				{
					title:"System",
					route:"System",
				},
				{
					title:"Initiative",
					route:"Initiative",
				},
				{
					title:"Department",
					route:"Department",
				},
			])
		}
]}