import SelectList from "./dataComponents/SelectList.js";
import NavOptions from "./dataComponents/NavOptions.js";
import HeaderComp from "./dataComponents/HeaderComp.js";
import Button from "./dataComponents/Button.js";
import Text from "./dataComponents/Text.js";
import SearchBar from "./dataComponents/SearchBar.js";

const Styles = {
	btn:{
		marginRight:10,
	}
}

export default {
	navigationOptions:NavOptions("Request Feedback", true),
	state:{users:[
	 		{
	 			title:"Alec",
	 			value:'alec',
	 		},
	 		{
	 			title:"Alex",
	 			value:'alex',
	 		},
	 		{
	 			title:"Dan",
	 			value:'dan',
	 		},
	 		{
	 			title:"Natalie",
	 			value:'natalie',
	 		},
	 		{
	 			title:"Robin",
	 			value:'robin',
	 		},
	 	]},
	blocks:[
	HeaderComp("",{
			right:{
				icon:"close",
				route:"Home"
			}
	}),
	{
		"component":"H1",
		"slot":"Who?",
	},
	SearchBar(),
	{
		"component":"ScrollView",
		'horizontal':true,
		subcomponents:[
			Button("Suggested",{style:Styles.btn, transparent:true}),
			Button("All",{style:Styles.btn, transparent:true}),
			Button("Favorites",{style:Styles.btn, transparent:true}),
			Button("My Groups",{style:Styles.btn, transparent:true}),
			Button("Org Groups",{style:Styles.btn, transparent:true}),
		]
	},
	{
    "component":"ScrollView",
	 subcomponents:SelectList("@users")
	}]
}