import LinkList from "./dataComponents/LinkList.js"
import Image from "./dataComponents/Image.js"
import Text from "./dataComponents/Text.js"
import NavOptions from './dataComponents/NavOptions.js'
import HeaderComp from './dataComponents/HeaderComp.js'
export default {
	navigationOptions:NavOptions("Help",true),
	blocks:[
		HeaderComp("Help",{
			right:{
				icon:"close",
				route:"Home"
			}
		}),
	]
}