 # Install

```
yarn
```

# Serve

Currently developing with expo.io (likely to change)

```
expo start
```
Hit "w" after it's started to launch the web application.

# While Developing ( Important )

To utilize automated screen generation & instant code validation, you have to run the watcher script:

```
yarn watch
```


# Screen Files
```
.ProjectRoot
|	+-- src
|   	+-- screens

```
Files in the "screens" directory are used to configure generated view files. Screen files must output an object with a "blocks" array that contains at least one object with a "component" prop.

Example:
HelloWorld.js
```
export default {
	blocks:[
		{
			component:"Text",
			slot:"Hello World!"
		}
	]
}

``` 

You can also configure other elements on a screen file such as state:

```
export default {
	state:{
			first_name:"Marty",
			last_name:"McFly"
		},
	blocks:[
		{
			component:"Text",
			slot:"Hello @@first_name @@last_name"
		}
	]
}

```

or set navigation and header options:

```
export default {
	navigationOptions:{
	    title: 'Hello',
	    headerStyle: {
	      backgroundColor: '#f4511e',
	    },
	    headerTintColor: '#fff',
	    headerTitleStyle: {
	      fontWeight: 'bold',
	    },
	},
	state:{
			first_name:"Marty",
			last_name:"McFly"
		},
	blocks:[
		{
			component:"Text",
			slot:"Hello {state.first_name} {state.last_name}" //(DEV NOTE: Rendering is in progress)
		}
	]
}
```


## Screen & Block Templates
```
.ProjectRoot
|	+-- src
|   	+-- screens
|   		+-- templates
```
Screen template files contain classes and methods designed to reduce code.

Example:
LinkList.js (This will render a list of links)
```
const style = {
	body:{
		flex:1
	},
	listItem:{
		fontsize:20,
	}
};

function LinkList(Links){
	var array = [];
	for (var i = Links.length - 1; i >= 0; i--) {
		var item = 
				{
				component:"ListItem",
				style:style.listItem,
				navigate:Links[i].route,
				subcomponents:[
					{
						component:"CardItem",
						subcomponents:[
							{
								component:"Icon",
								"name":Links[i].icon || "arrow-forward"
							},
							{
								component:"View",
								"subcomponents":[
									{
										component:"Text",
										"slot":Links[i].title,
										"style":{
											"fontSize":style.fontsize,
										}
									},
								]
							}
						]
					}
				]
			};
			array.push(item)
	}
	return array;
}

export default LinkList;
```

use:
```js
var links = [
				{
					title:"My Feedback",
					route:"Settings",
				},
				{
					title:"Give Feedback",
					route:"Settings",
				},
			];
LinkList(links);
```

- !All screen template function's last argument should be optional override props or additional props

## Screen Blocks
Screen blocks are arrays that contain objects that specify a series of component and their accompanying props. 
Props can be anything a typical javascript object will allow, strings, numbers, booleans, arrays, objects, and functions.
```
blocks:[
	{
		"component":"Image",
		"key_name":"image",
		"style":{"height": 200, "width": 200, "flex": 1},
		"source":{"uri": "https://outmatch.com/wp-content/uploads/2018/09/dm-logo.png"}
	},
	{
		"component":"Button",
		"navigate":"Dashboard",
		"key_name":"signin",
		"subcomponents":[
		{
			"component":"Text",
			"slot":"Sign In",
			"style":{"color":"white"},
			"key_name":"signinText"
		}
		],
		"style":{"color":"white",
		"flex":1,
		"marginTop":50,
		"justifyContent": "center",
		"alignItems": "center"},
	},
]
```

## "@@" variables in strings
You can access state variables by adding the prefix "@@" to the state variables name.

```
{
	"component":"Text",
	key_name:"helloText",
	"slot":"Hello @@first_name",
}
```
OR
```
{
	"component":"Text",
	key_name:"ratingText",
	"slot":"@@labels[@@rating-1]",
}
```
anything connected to a word (variable) that starts with "@@" will be processed as javascript. 

## "#value" (event.target.value)

# Auto Generated View Files
The watcher script will keep track of various files in the project and generate view files accordingly.
```
.ProjectRoot
|	+-- src
|   	+-- components
|   		+-- Views
|   			+-- GeneratedViews
```

### Core Templates 
## Results
### Imports
### Constants
### Bottom of File
### View Index
```
.ProjectRoot
|	+-- src
|   	+-- components
|   		+-- Views
|   			+-- index.js
```
This file will be generated / updated whenever a new custom view file is added, a file in the screen directory is deleted, or a new file in the screen directory has been created and passes requirements needed to generate a view file. 

# Custom View Files
```
.ProjectRoot
|	+-- src
|   	+-- components
|   		+-- Views
```
Any view component written in the "Views" directory outside of it's "GeneratedViews" subdirectory.


# Components
## Dynamic Component
```
.ProjectRoot
|	+-- src
|   	+-- components
|   		+-- core
|   			+-- BasicDynamicComponent.js
```
The component that renders all components that are passed to it. This is the main component that gets used in the auto generated view files. 

## Trusted Components 
Trusted components are components that are provided by trusted / tested / widely used 3rd party libraries.

### Current trusted libraries
- native-base
- The React Native component library

### Registering a new trusted library

## Custom Components 
- Registering a new Custom Component


# Real-Time Code Validation
- Syntax
- Live Data
- Recursive
- Project Specific Validation
	- Non-registered components warnings
	- Missing required objects in screen files warnings
- Terminal Notifications
	- Terminal Beep

# TODO
- styleSheet
	- variables.js (native-base)
- Passing @state <-> @nav 
- shouldComponentUpdate / PureComponents (react)
- Fix toast on Dynamic Component
- Fix animation on Dynamic Component
- navigation validation (watcher)
- prop based validation (watcher)
- "on the fly" testing? (watcher) (jest integration)

# Known Issues
- Watcher breaks on new or deleted "screen/dataComponent" files.