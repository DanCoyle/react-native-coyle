import Watcher from "./watcher/Watcher.js";
const Watch = new Watcher
const colors = Watch.colors();
const directories = Watch.directories();

process.argv.forEach((val, index) => {
  console.log(`${index}: ${val}`)
})

/**
 * On the initial start of this script all of the files in the "screens" directory will
 * be used to generate all of the files in the "components/Views/generatedViews" directory.
 */
console.log(colors.eventColor,"\n\t\tSyncing Screen Directory with Generated Views Directory:")
console.log(colors.warning,"\tScreen Directory: ");
console.log("\t"+directories.screenDirectory)
console.log(colors.warning,"\tGenerated Views Directory: ");
console.log("\t"+directories.generatedViewDirectory);
console.log("\n\t::: The files that are contained in the 'src/screens' directory "
	+"\n\t::: will populate the files in the 'src/components/views/generated-views' directory."+
	"\n\t::: The top of the generated view files are created by the class found in 'watcher/Watcher.js'."+
	"\n\t::: All the bottom of the file is an exact copy of 'src/components/core/BottomOfGeneratedFile'")
	Watch.updateDirectory();


/**
 * The screens directory will be monitored for changes. Any changes in the screens directory will update the generated view files as well as their index.
 */
console.log(colors.eventColor,"\n\t\tNow watching 'screens' directory for changes...");
console.log("\n\t::: Any change events to the files in the 'src/screens' directory will create,"+
	"\n\t::: update or delete it's corresponding file in the 'src/components/views/generated-views' directory"+
	"\n\t::: If a file is added or deleted, the 'src/components/views/index.js' file will also be updated.")
	Watch.watchScreenDir()

/**
 * If custom views are added to the view directory, they are indexed so they can be accessed via the navigation.
 */
console.log(colors.eventColor,"\n\t\tNow watching 'components/views' directory for changes...");
console.log("\n\t::: New, renamed, or deleted file events that happen in the 'src/components/views' directory"+
	"\n\t::: outside of the 'generated-views' folder will update the 'src/components/views/index.js'")
	Watch.watchViewDirectory()

/**
 * There is a file used as the template for bottom of the generated view files. If this file is changed it will update run Watch.updateDirectory again.
 */
console.log(colors.eventColor,"\n\t\tNow watching 'components/core/BottomOfGeneratedFile.js' file for changes...");
console.log("\n\t::: If the contents of 'src/components/core/BottomOfGeneratedFile.js' are changed"+
	"\n\t::: then all the files in the 'src/components/views/generated-views' will be updated.")
	Watch.watchGeneratedViewContentFile()

/**
 * There is a file used as the template for bottom of the generated view files. If this file is changed it will update run Watch.updateDirectory again.
 */
console.log(colors.eventColor,"\n\t\tNow watching 'screens/styles/Styles.js' file for changes...");
console.log("\n\t::: Provides additional validation and hints on react-native style rules."+
"\n\t::: create a style object in 'screens/styles/Styles.js' and write 'help' as a rule value"+
"\n\t::: and the terminal will show you information on that rule: example: {color:'help'}");
console.log(colors.warning,"\n\t###Project Notes###")
console.log(colors.warning,"\tStyle watcher running very slow, too much in the 'watcher/styleHelper.js' file?.")
console.log(colors.warning,"\tMaybe something like this: \n\thttps://stackoverflow.com/questions/22646996/how-do-i-run-a-node-js-script-from-within-another-node-js-script")
console.log("\n\n");
	Watch.watchStyleFile()

console.log(colors.error,"\t\tKEEP AN EAR OUT FOR THE BEEP!","\x1b[0m"," HAPPY CODING :)")
console.log("\n\n");