import os

path = os.getcwd()+"\\meta"

files = []
# r=root, d=directories, f = files
for r, d, f in os.walk(path):
    for file in f:
        if '.js' in file and 'index.js' != file and '\\builders' not in r:
            files.append(file)

out = "";
for f in files:
    out = out + "export "+f.replace(".js","")+" from './"+f+"'\n";

f = open(path+"\\index.js", "w")
f.write(out)
f.close();