import React from 'react';
import * as Font from 'expo-font';
import {Navigator} from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import * as Screens from './src/components/views';
import 'react-native-gesture-handler'

const screens = () => {
	let output = {};
	let screensKeys  = Object.keys(Screens).reverse();
	for (var i = screensKeys.length - 1; i >= 0; i--) {
		let key = screensKeys[i];
		output[key] = {};
		output[key].screen = Screens[key];
	}
	return output;
}
const AppNavigator = createDrawerNavigator(screens(),{ initialRouteName: 'FeedbackForm' });
export default createAppContainer(AppNavigator);