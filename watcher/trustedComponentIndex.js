const trustedComponents = {
	'native-base':[
		"Accordion", "Textarea", "ActionSheet", "Anatomy", "Badge", "Body", "Button", "Card", "CardItem", "CheckBox", "Container", "Content", "DatePicker", "DeckSwiper", "Drawer", "FABs", "Footer", "FooterTab", "Form", "H1", "H2", "H3", "Header", "Icon", "Input", "Item", "Layout", "Left", "List", "ListItem", "Picker", "Radio", "Ref", "Right", "SearchBar", "Segment", "Spinner", "Subtitle", "SwipeableList", "Tabs", "Text", "Thumbnail", "Title", "Toast", "View"],
	'react-native':[
		"Image", "ScrollView", "Slider", ],
	'react-native-easy-grid':[
		"Col","Row","Grid"
	]
}

export default trustedComponents;